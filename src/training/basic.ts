// number => 1, 5.3, 10
// string => 'Hi', "Hi", `Hi`
// boolean => true, false | truthy, falsy

function add(
  n1: number,
  n2: number,
  showResult: boolean,
  resultPhrase: string
) {
  if (showResult) {
    console.log(`${resultPhrase}${n1 + n2}`);
  }
  return n1 + n2;
}

let number1: number;
number1 = 5;
number1 = 10;
const number2 = 2.8;
const printResult = true;
const resultPhrase = 'Result is: ';

add(number1, number2, printResult, resultPhrase);

// Nested Objects & Types
const suleiman: {
  father: string;
  born: string;
  age: number;
} = {
  father: 'Selim I',
  born: 'Trabzon',
  age: 71,
};

console.log(`Magnificient Suleiman => was born in ${suleiman.born} `);

const product: {
  id: string;
  price: number;
  tags: string[];
  details: {
    title: string;
    description: string;
  };
} = {
  id: 'abc1',
  price: 12.99,
  tags: ['great-offer', 'hot-and-new'],
  details: {
    title: 'Red Carpet',
    description: 'A great carpet - almost brand-new!',
  },
};

console.log(`productTags => ${product.tags}`);
console.log(`productDetails => ${product?.details?.title}`);

// Enums
enum Authority {
  ADMIN,
  READ_ONLY,
  AUTHOR,
}

// Array types
const omer = {
  born: 'Mekke',
  father: 'Hattab bin Nüfeyl',
  mama: 'Hanteme bint Haşim',
  hobbies: ['ride a horse', 'fighting', 'wrestling'],
  role: [2, 'caliph'],
  authority: Authority.ADMIN,
};

omer?.hobbies.forEach((hobby) => {
  console.log(hobby.toUpperCase());
});

// Tuples
omer?.role.push('shepherd');
// omer?.role = [0, 'admin', 'user'];

console.log(`Omer's role => ${JSON.stringify(omer?.role)}`);

// Enum
if (omer?.authority === Authority.ADMIN) {
  console.log(`Omerin yetkisi => ${Authority.ADMIN} olarak gosterilir`);
}

// Any type so flexible that means any type
let favoriteActivities: any[];
favoriteActivities = ['Sports'];
favoriteActivities = ['Sports', 4, false];

// Union types and Literal Types
type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text';

function combine(
  input1: Combinable,
  input2: Combinable,
  resultConversion: ConversionDescriptor
) {
  let result: string | number;
  if (
    (typeof input1 === 'number' && typeof input2 === 'number') ||
    resultConversion === 'as-number'
  ) {
    result = +input1 + +input2;
  } else {
    result = `${input1.toString()} ${input2.toString()}`;
  }
  return result;
}

const combinedAges = combine(10, 25, 'as-number');
console.log(`combinedAges => ${combinedAges}`);

const combinedStringAges = combine('10', '25', 'as-number');
console.log(`combinedStringAges => ${combinedStringAges}`);

const combinedNames = combine('Mahmut', 'Unlu', 'as-text');
console.log(`combinedNames => ${combinedNames}`);

// Type Aliases
type User = { name: string; age: number };

function greet(user: User) {
  console.log(`Hi I am ${user.name}`);
}
function isOlder(user: User, checkAge: number) {
  return checkAge > user.age;
}

const u1: User = { name: 'Max', age: 30 };
// const u1Array: User = ['Thomas', 29]; // TS2739: Type '(string | number)[]' is missing the following properties from type 'User': name, age
