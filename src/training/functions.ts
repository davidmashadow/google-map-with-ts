// Function as parameters | void, undefined
function addPlus(n1: number, n2: number): number {
  return n1 + n2;
}

ciktiSonucu(addPlus(5, 12));

// Function as types
function ciktiSonucu(num: number): void {
  console.log(`ciktiSonucu: ${num}`);
}

let degerleriBirlestir: (a: number, b: number) => number;
degerleriBirlestir = addPlus;
degerleriBirlestir = addPlus;

console.log(degerleriBirlestir(10, 20));

// Function as callbacks
function addPlusAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

addPlusAndHandle(10, 20, (result) => {
  console.log(result);
  return result;
});

function sendRequest(data: string, cb: (response: any) => void) {
  // ... sending a request with "data"
  return cb({ data: 'Hi there!' });
}

sendRequest('Send this!', (response) => {
  console.log(response);
  return true;
});

// unknown
let userInput: unknown;
let userName: string;
userInput = 5;
userInput = 'Max';

if (typeof userInput === 'string') {
  userName = userInput;
}

// never
function generateError(message: string, code: number): never {
  throw new Error(`${message} | ${code}`);
}

generateError('An error occured', 500);

// wrap up
