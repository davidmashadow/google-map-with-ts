type CakeName = {
  readonly name: string;
  outputName?: string; // outputName constant it could be or couldn't be that doesn't matter...
};

interface CakePiece extends CakeName {
  slogan(phrase: string): void;
}

class CAKE implements CakePiece {
  name: string;

  packagingColor: string;

  brand: string;

  serialNo?: number;

  constructor(n: string, p: string, b: string, s?: number) {
    this.name = n;
    this.packagingColor = p;
    this.brand = b;
    if (s) {
      this.serialNo = s;
    }
  }

  slogan(phrase: string) {
    console.log(`${phrase} Hayatiniz icin ${this.name}`);
  }
}

let yeniKek: CakePiece;

yeniKek = new CAKE('Mozaik kek', 'red', 'CASEY', 9696067747163);
yeniKek = new CAKE('Dankek', 'blue', 'ULKER', 8695077007865);

console.log(`YENI KEK => ${JSON.stringify(yeniKek, undefined, 2)}`);

// Function Types
// type AddFn = (a: number, b: number) => number;

interface AddFn {
  (a: number, b: number): number;
}

const toplaBakalim: AddFn = (n1: number, n2: number) => {
  const result = n1 + n2;
  return result;
};

toplaBakalim(4, 5);
