const kullaniciAdi = 'Max';
let age = 30;
age = 29;
/*
function add(a: number, b: number) {
  let result;
  result = 10;
  result = a + b;
  return result;
}
*/
// console.log(result);

const haydiTopla = (a: number, b = 4) => a + b;

const toplamCiktisi: (a: number | string) => void = (output) =>
  console.log(output);

toplamCiktisi(haydiTopla(3));

// spread Operators
const hobilerim = ['Sports', 'Cooking'];
const aktiveHobilerim = ['Hiking'];
aktiveHobilerim.push(...hobilerim);

const turkiye = {
  bolge: 'Akdeniz',
  uzaklik: 300,
};

const kopyalaTurkiye = { ...turkiye };

console.log(aktiveHobilerim);
console.log(kopyalaTurkiye);

const birSeyDeneyecegim = (...numaralar: number[]) => {
  console.log(numaralar);
};

birSeyDeneyecegim(1, 2, 3, 4);

// Array and Object destructiring
// const [hobby1, hobby2, ...remainingHobbies] = hobbies;
// const { bolge: district, uzaklik: distance } = turkiye
