// Generics
// any[]   Array<string>   Array<string | number>   Array<any>
const isimler: Array<string> = [];

// Promise<string>   Promise<any>
const promise: Promise<number> = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(10);
  }, 2000);
});

promise
  .then((response) => {
    console.log(response);
    return null;
  })
  .catch((err): void => {
    console.log(err);
  });

type ObjA = {
  name: string;
};

type ObjB = {
  age: number;
};

// Generic Function
function objeBirlestirici<T extends ObjA, U extends ObjB>(objA: T, objB: U) {
  return Object.assign(objA, objB);
}

const birlestirilmisObje = objeBirlestirici(
  { name: 'Max', hobbies: ['Sports'] },
  { age: 30 }
);

console.log(birlestirilmisObje);

// Another Generic Function
interface Lenghty {
  length: number;
}

function countAndDescribe<T extends Lenghty>(element: T): [T, string] {
  let descriptionText = 'Got no value';
  if (element.length === 1) {
    descriptionText = 'Got 1 element';
  } else if (element.length > 1) {
    descriptionText = `Got ${element.length} elements`;
  }
  return [element, descriptionText];
}

// keyof Constrain
type ObjC = {
  name: string;
  surname: string;
};

function extractAndConvert<T extends ObjC, U extends keyof T>(obj: T, key: U) {
  return `Value ${obj[key]}`;
}

// extractAndConvert({ name: 'Max', surname: 'Stephan' }, 'Muller');

// Generic Classes

// Bu T isareti sinifin ya da fonksiyonun turunu biz belirledigimiz zaman kullaniriz
class DATASTORAGE<T> {
  private data: T[] = [];

  addItem(item: T) {
    this.data.push(item);
  }

  removeItem(item: T) {
    this.data.splice(this.data.indexOf(item), 1);
  }

  getItems() {
    return [...this.data];
  }
}

// Burada da string olacagini belirttik T stringi yedi asagiya aktardi hep T ozgurdur
// Generic Types kitler adami elini ayagini buker
const textStorage = new DATASTORAGE<string>();
textStorage.addItem('Suleyman');
textStorage.addItem('George');
textStorage.removeItem('George');
console.log(textStorage.getItems());

// Coklu deger de alabilir
const numberStorage = new DATASTORAGE<number | string>();

// Obje de olabilir
const objStorage = new DATASTORAGE<ObjC>();

// Generic Utility Types
interface CourseGoal {
  title: string;
  description: string;
  completeUntil: Date;
}

function createCourseGoal(
  title: string,
  description: string,
  date: Date
): CourseGoal {
  const courseGoal: Partial<CourseGoal> = {};
  courseGoal.title = title;
  courseGoal.description = description;
  courseGoal.completeUntil = date;
  return courseGoal as CourseGoal;
  // return { title, description, completeUntil: date };
}

const nameees: Readonly<string[]> = ['Max', 'Anna'];
// nameees.push('Manu');
// nameees.pop();

// const data = extractData<string>(user, 'userId');
