// eslint-disable-next-line max-classes-per-file
function Logger(logString: string) {
  console.log(logString); // LOGGING-PERSON
  return function <T>(constructor: T) {};
  console.log(logString); // Creating person object...
}

@Logger('LOGGING-PERSON')
class USERINFORMATION {
  name = 'Max';

  constructor() {
    console.log('Creating person object...');
  }
}

const userInfo = new USERINFORMATION();
console.log('-----------------');

// Diving into Property Decorators
// 1
function Log(target: any, propertyName: string | symbol) {
  console.log('PROPERTY DECORATOR!!!');
  console.log(`target => ${JSON.stringify(target, undefined, 2)}`);
  console.log(`propertyName => ${propertyName.toString()}`);
  console.log('-----------------');
}
// 2
function Log2(target: any, name: string, descriptor: PropertyDescriptor) {
  console.log('ACCESSOR DECORATOR!!!');
  console.log(`target => ${JSON.stringify(target, undefined, 2)}`);
  console.log(`name => ${name}`);
  console.log(`descriptor => ${JSON.stringify(descriptor, undefined, 2)}`);
  console.log('-----------------');
}
// 4
function Log3(
  target: any,
  name: string | symbol,
  descriptor: PropertyDescriptor
) {
  console.log('METHOD DECORATOR');
  console.log(`target => ${JSON.stringify(target, undefined, 2)}`);
  console.log(`name => ${name.toString()}`);
  console.log(`descriptor => ${JSON.stringify(descriptor, undefined, 2)}`);
  console.log('-----------------');
}
// 3
function Log4(target: any, name: string | symbol, position: number) {
  console.log('PARAMETER DECORATOR');
  console.log(`target => ${JSON.stringify(target, undefined, 2)}`);
  console.log(`name => ${name.toString()}`);
  console.log(`position => ${position}`);
  console.log('-----------------');
}

class MARKETPRODUCT {
  @Log
  title: string;

  private price: number;

  constructor(t: string, p: number) {
    this.title = t;
    this.price = p;
  }

  @Log2
  set setPrice(val: number) {
    if (val > 0) {
      this.price = val;
    } else {
      throw new Error('Invalid price - should be positive');
    }
  }

  @Log3
  getPrice(@Log4 tax: number) {
    return this.price * (1 + tax);
  }
}

const P1 = new MARKETPRODUCT('Book', 19);
const P2 = new MARKETPRODUCT('Book2', 25);
