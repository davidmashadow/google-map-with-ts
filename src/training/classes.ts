// eslint-disable-next-line max-classes-per-file
class EMPIRE {
  // public elemanlara disaridan erisebiliriz
  public wars: string[] = [];

  // protected korumalidir iceriye giren degerler degistirilemez
  protected conqueredCities: string[] = [];

  constructor(
    protected readonly id: number,
    private sultan: string,
    private capital: string,
    public foundingYear: number,
    protected totalSoldiers: number
  ) {}

  empireWars(this: EMPIRE): void {
    console.log(`Hukumdar => ${this.sultan} | Baskenti => ${this.capital}`);
  }

  // Osmanli devleti toplu savasa girip toplu sehir fethetmez ise bunun devlete bir etkisi olmayacktir
  letsFight(war: string | string[], city: string | string[]) {
    if (Array.isArray(war) && Array.isArray(city)) {
      this.wars.push(...war);
      this.conqueredCities.push(...city);
    }
  }

  // static bir method yazalim
  static createNewEmpire(value: string) {
    return {
      message: `${value} => Bu girmis oldugun deger Imparatorluk olusturmaya yetmez`,
    };
  }
}

// Yeni bir TIMURDEVLETI kuralim ve onu EMPIRE constructor() icerisindeki sabitlerden turetelim. super() icerisinde constructor(){} disindaki public disindaki sabitlere erisemedigimize dikkat edelim
class TIMURDEVLETI extends EMPIRE {
  // static | Nesne disarisindan erismek istedigimiz sabitleri tanimlamak icin kullaniriz
  static timurOlumTarihi = 1405;

  // private olan elemanlari sadece iceriden kontrol edebilecegimizi unutmayalim disaridan erisemeyiz
  private fatality: string;

  private families: string[];

  private bestParent: string;

  // Getters and Setters Timurun ailesini yakindan inceleyelim
  get myBestParent() {
    if (this.bestParent) {
      return this.bestParent;
    }
    throw Error(`He don't have a bestParent`);
  }

  // Setterslar adinda anlasilacagi gibi bir parametreye ihtiyac duyarlar
  set myBestParent(value: string) {
    if (!value) {
      throw Error(`He don't have a bestParent`);
    }
    this.addBestParent(value);
  }

  // constructor nesne icerisindeki elemanlari kilitler
  constructor(
    wars: string[],
    conqueredCities: string[],
    fatality: string,
    families: string[]
  ) {
    // Super kalitim aldigi degerleri doldurmak zorundadir. Yoksa calismaz
    super(1, 'Timur', 'Semerkant', 1336, 40000);
    this.wars = wars;
    this.conqueredCities = conqueredCities;
    this.families = families;
    this.fatality = fatality;
    [this.bestParent] = families;
  }

  addBestParent(parent: string) {
    this.bestParent = parent;
  }

  addConqueredCities(city: string[]) {
    if (city.length > 0) {
      this.conqueredCities.push(...city);
    }
  }
}

// Timur devletine yeni ozellikler ekleyelim. Ana ozelliklerini EMPIRE uzerinden aldigini unutmayalim
const timurDevleti = new TIMURDEVLETI(
  ['Ankara Savasi', 'Delhi Savasi'],
  ['Dogu Turkistan', 'Iran', 'Azerbeycan'],
  'Elephants',
  ['Pir Muhammed', 'Halil Sultan']
);

// Timur devletine yeni sehirler ekleyelim
timurDevleti.addConqueredCities(['-- Irak --', '-- Suriye --']);

// TimurDevletinin Hukumdarini kontrol edelim. Bu ozellik TIMURDEVLETI nesnesinde tanimli olmadigi halde super() uzerinden aldigina dikkat edelim
timurDevleti.empireWars();

const Ottomans = new EMPIRE(
  2,
  'Mogol istilasindan kacan Osman',
  'Bursa-Sogut',
  1299,
  5000
);

// Osmanli beyligini toplu savasa sokalim ve 2 tane Sehir fethettirelim
Ottomans.letsFight(
  ['Pelekanon Savasi', 'Sazlidere Savasi'],
  ['Bilecik', 'Inegol']
);

// Getters calistiralim | Getterslar bir seyleri return ederler
// Mesela Timurun en yakin akrabasi families dizisinin ilk elemaniydi haydi gelin onu dondurelim
console.log(`Timurun en yakin akrabasi => ${timurDevleti.myBestParent}`);

// Setters | Timurun en yakin aile bireyini tanimlamadigim halde bu setters sayesinde tanimladim
timurDevleti.myBestParent = '-- Pir Muhammed --';

// Timur hangi yilda oldu ki
console.log(`Timurun Olum yili => ${TIMURDEVLETI.timurOlumTarihi}`);

// Yeni bir imparatorluk yaratmaya calisalim
const ISIS = EMPIRE.createNewEmpire('Ebu İbrahim el-Haşimi el-Kureyşi');
console.log(`ISIS Devleti => ${JSON.stringify(ISIS, undefined, 2)}`);

// Osmanliyi kontrol et
console.log(`Osmanli Beyligi => ${JSON.stringify(Ottomans, undefined, 2)}`);

// Timur Devletini kontrol edelim
console.log(`Timur Devleti => ${JSON.stringify(timurDevleti, undefined, 2)}`);

// abstract class ve methods ayrica private constructor kavramlarina da bak
// A class that can't be instantiated but has to be extended
// You only ever have one instance of a singleton class
