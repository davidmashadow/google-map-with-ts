// eslint-disable-next-line max-classes-per-file
type Admin = {
  name: string;
  privileges: string[];
};

type Employee = {
  name: string;
  startDate: Date;
};

// Also we can define Interface
// interface ElevatedEmployee extends Employee, Admin {...}

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
  name: 'Max',
  privileges: ['create-server'],
  startDate: new Date(),
};

// Type Guards
type Combinablee = string | number;
type Numericc = number | boolean;

type Universal = Combinablee & Numericc;

function ornekBirToplamaIslemi(a: Universal, b: Universal) {
  if (typeof a === 'string' || typeof b === 'string') {
    return a.toString() + b.toString();
  }
  return a + b;
}

ornekBirToplamaIslemi(3, 5);

type UnknownEmployee = Employee | Admin;

function printEmployeeInformation(emp: UnknownEmployee) {
  console.log(`Iscinin Adi => ${emp.name}`);
  if ('privileges' in emp) {
    console.log(`Yan haklar => ${emp.privileges}`);
  }
  if ('startDate' in emp) {
    console.log(`Is baslangic => ${emp.startDate}`);
  }
}

printEmployeeInformation(e1);
printEmployeeInformation({ name: 'Manu', startDate: new Date() });
// We don't need to send all Data

class CAR {
  name: string;

  model: string;

  constructor(vehicleName: string, vehicleModel: string) {
    this.name = vehicleName;
    this.model = vehicleModel;
  }

  drive(this: CAR): void {
    console.log(`${this.name} | ${this.model} is driving Car...`);
  }
}

class TRUCK extends CAR {
  amount: number;

  constructor(vehicleAmount: number) {
    super('Volvo', 'D315');
    this.amount = vehicleAmount;
  }

  drive(this: CAR): void {
    console.log(`${this.name} | ${this.model} is driving Car...`);
  }

  loadCargo(amount: number): void {
    console.log(
      `${this.name} | ${this.model} is driving Car... This driver carrying ${amount}`
    );
  }
}

const Ford = new CAR('Nuri', 'Mondeo');
const Volvo = new TRUCK(500);

Ford.drive();
Volvo.loadCargo(2500);

type Vehicle = CAR | TRUCK;

function useVehicle(vehicle: Vehicle) {
  vehicle.drive();
  if (vehicle instanceof TRUCK) {
    vehicle.loadCargo(1000);
  }
}

// Discriminated Unions
interface Bird {
  type: 'Bird';
  flyingSpeed: number;
}

interface Horse {
  type: 'Horse';
  runningSpeed: number;
}

type Animal = Bird | Horse;

function moveAnimal(animal: Animal) {
  let speed;
  switch (animal.type) {
    case 'Bird':
      speed = animal.flyingSpeed;
      break;
    case 'Horse':
      speed = animal.runningSpeed;
      break;
    default:
      break;
  }
  console.log(`Moving at speed: ${speed}`);
}

// You can't assign runningSpeed
moveAnimal({ type: 'Bird', flyingSpeed: 10 });

// Type Casting
// const userInputElement = <HTMLInputElement>document.querySelector('user-input');
// const userInputElement = document.getElementById('user-input') as HTMLInputElement;
